const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({

	//required field will allow us to require the field and disallow creating the document if the field required is missing or missing a value.
	//default field will allow us to add a default value for our document field. This will allow us to create a document without the need to add a value for the field.
	name: {
		type: String,
		required: [true,"Name is required."]
	},
	description: {
		type: String,
		required: [true, "Description is required."]
	},
	price: {
		type: Number,
		required: [true, "Price is required."]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	enrollees: [

		{
			userId: {
				type: String,
				required: [true, "User Id is required"]
			},
			dateEnrolled: {
				type: Date,
				default: new Date()
			},
			status: {
				type: String,
				default: "Enrolled"
			}
		}


	]

})

module.exports = mongoose.model("Course",courseSchema);