const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers")

const auth = require("../auth");
const {verify,verifyAdmin} = auth;



// routes

// create/add course
// Since this route should not be accessed by all users, we have to authorize only some logged in users who have the proper authority:
// First, verify if the token the user is using is legit.
router.post("/",courseControllers.addCourse);


// get all courses
router.get("/",courseControllers.getAllCourses);


// get single course
router.get('/getSingleCourse/:id', courseControllers.getSingleCourse);


// update course
router.put('/:id',verify,verifyAdmin, courseControllers.updateCourse);











// ACTIVITY 4

router.get('/archive/:id',verify,verifyAdmin, courseControllers.archiveCourse);

router.put('/activate/:id',verify, verifyAdmin, courseControllers.activateCourse);

router.get('/getActiveCourses',courseControllers.getActiveCourses);



module.exports = router;