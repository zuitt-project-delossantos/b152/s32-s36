const express = require("express");
const router = express.Router();

// import user controllers
const userControllers = require("../controllers/userControllers")

// UserControllers is a module we imported. In JS, modules are objects.
// Exported controllers are methods to their modules.
// console.log(userControllers)

// for User Registration:
router.post("/",userControllers.registerUser);


// user login
router.post('/login', userControllers.loginUser);

router.get('/',userControllers.getAllUsers);


router.get('/getUserDetails', verify, userControllers.getUserDetails);












router.get('/getUserDetails',verify,userControllers.getUserDetails);

router.post('/checkEmailExists',userControllers.checkEmailExists);



module.exports = router;