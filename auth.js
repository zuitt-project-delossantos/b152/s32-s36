/*
	We will create our own module which will have methods that will help us authorize our users to access or to disallow access to certain parts/features in our app.
*/


// imports
const jwt = require("jsonwebtoken");
const secret = "CourseBookingApi";

/*
	JWT is a way to securely pass information from one part of a server to the frontend or other parts of our application. This will allow us authorize our users to access or disallow access to certain parts of our app.

	JWT is like a gift wrapping serving able to encode our user details which can only be uwrapped by jwt's own methods and if the secret privided is intact.

	If the jwt seemed tampered with we will reject the users attempt to access a feature in our app.
*/

module.exports.createAccessToken = (user) => {


	// check if we can receive the details of the foundUser in our login:
	// console.log(user);
	// data object is created to contain some details of our user
	const data = {

		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	// console.log(data);
	// create our jwt with data payload, our secret and the algorithm to create our JWT token

	return jwt.sign(data, secret,{});

}



module.exports.verifyAdmin = (req,res,next) => {


	// verifyAdmin comes after the verify middleware,
	// Do we have any access to our user's isAdmin detail?
	// We can get details from req.user because verifyAdmin comes after verify method.
	// NOTE: you can only have req.user for any middleware or controller that comes after verify.

	// console.log(req.user);

	if(req.user.isAdmin){
		// if the logged in user, based on his token is an admin, we will proceed to the next middleware/controller
		next();
	} else {
		return res.send({
			auth:"Failed",
			message:"Action Forbidden"
		})
	}

}