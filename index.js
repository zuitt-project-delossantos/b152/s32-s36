const express = require("express");
const mongoose  = require("mongoose");
const app = express();
const port = 4000;

//connect api to db:
mongoose.connect("mongodb+srv://admintj:admin123@cluster0.iu5up.mongodb.net/bookingAPI152?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	})

//notifications for successful or failed mongodb connection:
let db = mongoose.connection;
db.on("error",console.error.bind(console, "Connection Error"));
db.once("open",()=>console.log("Connected to MongoDB"));

app.use(express.json());


// import route from userRoutes
const userRoutes = require('./routes/userRoutes');
// use our routes and group together under '/users'
app.use('/users',userRoutes);



// import route from courseRoutes


// use our routes and group together under 



app.listen(port,()=>console.log(`Server running at localhost:4000`));