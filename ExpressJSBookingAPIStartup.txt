Create a new ExpressJS API:
	
	Create a new folder called: s32-s36
	Create an index.js file inside your s32-s36 folder.
	Open git bash in your s32-s36 folder and create a new package.json
	using npm init -y
	Install express, mongoose and nodemon into our new ExpressJS API.
	npm install <package name>

	Create a controllers folder inside your s32-s36 folder:
		Create a userControllers.js file in your controllers folder.
		Create a courseControllers.js file in your controllers folder.
	Create routes folder inside your s32-s36 folder
		Create a userRoutes.js file in your routes folder.
		Create a courseRoutes.js file in your routes folder.
	Create a models folder inside your s32-s36 folder
		Create User.js file inside the models folder.
		Create Course.js file inside the models folder.
		
	Add a start script to your package.json
		"start": "node index"
	Add a dev script to your package.json
		"dev": "nodemon index"
	Create a new ExpressJS API in the index.js file:
		-server should listen to port 4000,
		-show a message on the terminal "Server Running on localhost:4000" once the server is started and assigned to port 4000.