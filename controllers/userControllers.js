// import User Model
const User = require("../models/User");


// import bcrypt
const bcrypt = require("bcrypt");
// bcrypt is a package which will help us add a layer of security for our user's passwords.


// auth module
const auth = require("../auth");
// console.log(auth);


module.exports.registerUser = (req,res) => {

	
	// Does this controller need user input?
	// yes.
	// How/Where can we get our user's input?
	// req.body
	console.log(req.body);

	/*
		bcrypt adds a layer of security to our user's password.

		What bcrypt does is hash our password into a randomized character version of the original string.

		It is able to hide your password within that randomized string.

		syntax:
		bcrypt.hashSync(<strongToBeHashed>,<saltRounds>)

		Salt-Rounds are the number of times the characters in the hash are randomized
	*/


	// Create a variable to store the hashed/encrypted password. Then this new hashed pw will be saved in our database.
	const hashedPW = bcrypt.hashSync(req.body.password,10);
	console.log(hashedPW);


	// Create a new user document out of our User model:
	let newUser = new User({

		// Check your model, check the fields you need in your new document.
		// Check your req.body if you have all the values needed for each field.
		// Add hashedPW to add the hashed password in the new document.

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		mobileNo: req.body.mobileNo,
		password: hashedPW

	})


	// newUser is our document created out of our User model.
	// new documents created out of the User model have access to some methods.
	// .save() will allow us to save our new document into the collection that our model is connected to.
	// NOTE: do not add a semicolon to your then() chain
	newUser.save()
	.then(user => res.send(user))
	.catch(err => res.send(err));

}

module.exports.getAllUsers = (req,res) => {

	User.find({})
	.then(result => res.send(result))
	.catch(error => res.send(errpr));
}



module.exports.loginUser = (req,res) => {

	// Does this controller need user input?
	// yes
	// Whre can we get this user input?
	// req.body
	console.log(req.body); //contains user credentials

	/*
		1. find the user by the email
		2. If we found a user, we will check his password.
		3. If we don't find a user, we will send a message to the client.
		4. If upon checking the found user's password is the same as our input password, we will generate "key" to access our app. If not, we will turn him away by sending a message to the client.
	*/

	User.findOne({email: req.body.email})
	.then(foundUser => {

		// console.log(foundUser); is a found document(object)
		if(foundUser === null){
			return res.send("No User Found.")
		} else {
			// if we find a user, the foundUser parameter, will contain the details of the document that matched our req.body.email
			const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password);

			console.log(isPasswordCorrect);


			if(isPasswordCorrect){
				/*
					auth.createAccessToken will receive our foundUser as an argument. Then, in the createAccessToken() will return a "token" or "key" which contains some of our user's details.

					This key is encoded and only our own methods will be able to decode it.

					auth.createAccessToken will return a JWT token out of our user details. Then, with res.send() will be able to send a token to our client.

					This token or key will now let us allow or disallow certain users to access certain features of our app depending if they are admin or non-admins.
				*/
				return res.send({accessToken: auth.createAccessToken(foundUser)})

			} else {
				return res.send("Incorrect Password.")
			}

		}
	})

	.catch(err => res.send(err));
}









module.exports.checkEmailExists = (req,res) => {

	// console.log(req.body.email); //check if you can receive the email from our client's request body.
	// You can use find({email: req.body.email}), however, find() returns multiple documents in an ARRAY.



	User.findOne({email: req.body.email})
	.then(result => res.send(result))
	.catch(err => res.send(err));

}