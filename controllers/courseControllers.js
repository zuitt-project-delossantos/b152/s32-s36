// import Course Model
const Course = require("../models/Course");



module.exports.addCourse = (req,res) => {

	console.log(req.body);



	let newCourse = new Course({


		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		isActive: req.body.isActive

	});



newCourse.save()
	.then(course => res.send(course))
	.catch(error => res.send(error));


}


module.exports.getAllCourses = (req,res) => {

	Course.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));
}







module.exports.getSingleCourse = (req,res) => {

	// console.log(req,params.id) - check if you can receive the data coming from the url.


	Course.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))

}


// update course
module.exports.updateCourse = (req,res) =>{

	// console.log(); //Where can we get our course's id?


	// updates object will contain the field/fields to update and its new value.
	let updates = {

		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	// use findByIdAndUpdate() to update our course
	Course.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err));

}

















// ACTIVITY 4


module.exports.archive = (req,res) =>{

	
	let updates = {

		isActive: false
	}

	
	Course.findByIdAndUpdate(req.params.id,updates,{new:false})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err));

}




module.exports.activate = (req,res) =>{

	
	let activate = {

		isActive: true
	}

	
	Course.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(activatedCourse => res.send(activatedCourse))
	.catch(err => res.send(err));

}


module.exports.getActiveCourses = (req,res) => {

	Course.find({isActive:true})
	.then(result => res.send(result))
	.catch(error => res.send(error));
}

